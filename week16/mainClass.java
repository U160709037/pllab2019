import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class mainClass {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		HashMap<String, Integer> firstHash = new HashMap<>();
		HashMap<String, Integer> secondHash = new HashMap<>();
		
		Scanner input;
		
		String fileName;
		File rFile;
		
		
		
		
		input = new Scanner(System.in);
		
		System.out.println("Enter the path for the first file: ");
		fileName = input.nextLine();
		rFile = new File(fileName);
		addLinesToHash(rFile, firstHash);
		//At this point the first file is taken and its strings are entered in hashmap with line numbers.
		
		
		System.out.println("Enter the path for the second file: ");
		fileName = input.nextLine();
		rFile = new File(fileName);
		addLinesToHash(rFile, secondHash);
		//At this point the second file is taken and its strings are entered in hashmap with line numbers.
		
		input.close(); // No more input will be taken, so we close the scanner in order to not have source leak
		
		
		  
		  

		
		
		
		System.out.println("Line Numbers of the Missing Strings of the First File are : ");
		
		//This is the for each loop for hashMaps. This loop will do whats inside the brackets, 
		//for each entry in first hashmap.
		for(Map.Entry<String, Integer> entry : firstHash.entrySet()) {
		    String key = entry.getKey(); // get the key of the entry
		    //if the second hash does not include this string(key)
		    if(!secondHash.containsKey(key)) {
		    	int value = entry.getValue();
		    	//Then print the line number of this String
		    	System.out.println(value);
		    }
		}
		
	}
	
	
	
	
	 
	 
	 
	 
	 
	
	private static void addLinesToHash(File file, HashMap<String, Integer> hashMap) throws FileNotFoundException{
		//Scanner variable is declared to scan the given file
		Scanner fileScanner = new Scanner(file);
		int line = 1; 
		
		while (fileScanner.hasNext()) {
			String temp = fileScanner.nextLine(); 
			hashMap.put(temp, line);			 
			line++; 							
		}
		fileScanner.close(); // We are done with the scanner, so we close the scanner in order to not have source leak
	}
}
