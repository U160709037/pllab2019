use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input;


my $regex = '^\s*$';
$regex = '^[A-Z]+$';               #Accept only Capital Letters
$regex = '[A-Z]\d*/';              #star means 0 or more digits 
$regex = '^\d+\.\d+$';             #Only float numbers
$regex = '^(\d{1,3}\.){3}\d{1,3}$';            # . means any of character       
                                             # for curly brackets means min,max digits.      
 
until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}                                                                                     
