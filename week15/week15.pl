use strict ;
use warnings ;

my $filename = $ARGV[1];

open IN, '<',$filename or die "Can not open $filename!\n";

my @lines = <IN>;

close IN;

foreach my $line (@lines) {
chomp $line;
print $line, "\n";

}
